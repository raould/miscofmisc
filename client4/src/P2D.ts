import * as K from './K';
import * as V from './V2D';

type _P<_V> = { pos: _V, vel: _V, acc: _V };
export type W = K.W & _P<V.W>;
export type S = K.S & _P<V.S>;

export const mkW = (pv:V.W, vv:V.W, av:V.W):W => {
  return { pos: pv, vel: vv, acc: av } as W;
};
export const zeroW = ():W => mkW(V.zeroW(), V.zeroW(), V.zeroW());

export const mkS = (pv:V.S, vv:V.S, av:V.S):S => {
  return { pos: pv, vel: vv, acc: av } as S;
};
export const zeroS = ():S => mkS(V.zeroS(), V.zeroS(), V.zeroS());

export const w2s = (w:W):S => {
  return mkS(V.w2s(w.pos), V.w2s(w.vel), V.w2s(w.acc));
};

export const s2w = (s:S):W => {
  return mkW(V.s2w(s.pos), V.s2w(s.vel), V.s2w(s.acc));
};

export interface IHasWP2D {
  getWP2D():W;
}
