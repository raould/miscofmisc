import { incr, W as WV } from './V2D';
import { IHasWP2D, W, zeroW } from './P2D';
import { step } from './Phys2D';

export class Player 
implements IHasWP2D {
  readonly pt:W;

  constructor() {
    this.pt = zeroW();
  }

  getWP2D():W {
    return this.pt;
  }

  acc(a:WV) {
    this.pt.acc = incr(this.pt.acc, a);
  }

  update(dt:number) {
    step(this.pt, dt);
  }
}
