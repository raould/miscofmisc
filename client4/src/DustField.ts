import Sprite from 'openfl/display/Sprite';
import * as K from './K';
import * as V from './V2D';
import { IHasWP2D } from './P2D';
import { Dust } from './Dust';

export class DustField extends Sprite {

  readonly h_count:number;
  readonly v_count:number;

  constructor() {
    super();
    this.h_count = Math.ceil(K.w_width / K.dust_wstep);
    this.v_count = Math.ceil(K.w_height / K.dust_wstep);
    const count = this.h_count * this.v_count;
    for(let i = 0; i < count; ++i) {
      const d = new Dust();
      d.x = Number.MIN_VALUE;
      d.y = Number.MIN_VALUE;
      this.addChild(d);
    }
  }

  update(p:IHasWP2D) {
    const pw = p.getWP2D();
    for(let iy = 0; iy < this.v_count; ++iy) {
      for(let ix = 0; ix < this.h_count; ++ix) {
        const i = ix + (iy * this.h_count);
        if (i < this.numChildren) {
          // todo: mod by something so they always fill viewport.
          const w = V.incr(V.zeroW(), V.scale(pw.pos, -1));
          const s = V.w2s(w);
          const d = this.getChildAt(i);
          d.x = K.w2s(w.x + (ix * K.dust_wstep));
          d.y = K.w2s(w.y + (iy * K.dust_wstep));
        }
      }
    }
  }
}
