import * as V from './V2D';
import * as P from './P2D';

export const step = (w:P.W, dt:number):void => {
  const vdt = V.scale(w.vel, dt);
  const adt = V.scale(w.acc, dt);
  w.pos = V.incr(w.pos, vdt);
  w.vel = V.incr(w.vel, adt);
  w.acc = V.zeroW();
};

