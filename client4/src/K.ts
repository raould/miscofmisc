export declare enum W {};
export declare enum S {};

// match: index.html should rougly match this aspect ratio.
// todo: get SHOW_ALL to work, i want remove this hard-coded scale hack.
// todo: handle runtime display density.
// todo: handle display-appropriate letterboxing.
export const scale = 1/3;
export const w2s = (w:number):number => w * scale;
export const s2w = (s:number):number => s / scale;

// units: logical 2D world units.
export const w_width = 1024;
export const w_height = 768;
// units: physical 2D screen units.
export const s_width = w2s(w_width);
export const s_height = w2s(w_height);

export const dust_wstep = Math.max(w_width, w_height) / 10;

