import * as K from './K';

type _V = { x: number, y: number };
export type W = K.W & _V;
export type S = K.S & _V;

export const scale = <T extends _V>(v:T, scale:number):T => {
  return { x: v.x * scale, y: v.y * scale } as T;
}

export const incr = <T extends _V>(a:T, b:T):T => {
  return { x: a.x+b.x, y: a.y+b.y } as T;
};

export const mkW = (xn:number, yn:number):W => {
  return { x: xn, y: yn } as W;
};
export const zeroW = ():W => mkW(0, 0);

export const mkS = (xn:number, yn:number):S => {
  return { x: xn, y: yn } as S;
};
export const zeroS = ():S => mkS(0, 0);

export const w2s = (w:W):S => {
  return mkS(K.w2s(w.x), K.w2s(w.y));
};

export const s2w = (s:S):W => {
  return mkW(K.s2w(s.x), K.s2w(s.y));
};
