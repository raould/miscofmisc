import Sprite from 'openfl/display/Sprite';
import Bitmap from 'openfl/display/Bitmap';
import BitmapData from 'openfl/display/BitmapData';
import * as K from './K';

export class Dust
extends Sprite {
  constructor () {
    super ();
    // todo: should i cache bitmap data to share across instances?
    BitmapData
      .loadFromFile("particle.png")
      .onComplete((data) => {
        this.addChild(new Bitmap(data));
      });
  }
}


