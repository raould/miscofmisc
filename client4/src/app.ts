import Sprite from 'openfl/display/Sprite';
import Stage from 'openfl/display/Stage';
import Event from 'openfl/events/Event';
import * as K from './K';
import * as V from './V2D';
import { Player } from './Player';
import { DustField } from './DustField';

class App extends Sprite {
  readonly p:Player;
  readonly df:DustField;
  lastTime:number;

  constructor () {
    super ();
    this.p = new Player();
    this.df = new DustField();
    this.lastTime = Date.now();
    this.addChild(this.df);
    this.addEventListener(Event.ENTER_FRAME, this.update.bind(this));
  }

  update() {
    const now = Date.now();
    const dt = (now - this.lastTime)/1000;
    this.p.acc(V.mkW(3, 2))
    this.p.update(dt);
    this.df.update(this.p);
    this.lastTime = now;
  }
}

var stage = new Stage(K.s_width, K.s_height, 0xFFFFFF, App);
document.body.appendChild(stage.element);
