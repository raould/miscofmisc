import DisplayObjectContainer from 'openfl/display/DisplayObjectContainer';
import { K } from './K';
import { WV2D, SV2D } from './V2D';
import { ParticleSprite } from './ParticleSprite';

export class Field extends DisplayObjectContainer {
  ps:Array<ParticleSprite>;

  constructor() {
    super();
    this.ps = [new ParticleSprite()];
  }
}
