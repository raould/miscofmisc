import { K } from './K';
import { IV2Dmk, IV2D } from './IV2D';

export class V2Dutil {
  static s2w(s:SV2D):WV2D {
    return new WV2D(K.s2w(s.x), K.s2w(s.y));
  }
  static w2s(w:WV2D):SV2D {
    return new SV2D(K.w2s(w.x), K.w2s(w.y));
  }
}

export class WV2Dmk
implements IV2Dmk<WV2D> {
  zero():WV2D { return new WV2D(0, 0); }
}

export class SV2Dmk
implements IV2Dmk<SV2D> {
  zero():SV2D { return new SV2D(0, 0); }
}

export class V2D
implements IV2D {
  constructor(
	  public x: number,
	  public y: number
  ) {}
}

export class WV2D
extends V2D {
  constructor(x:number, y:number) {
    super(x, y);
  }
}

export class SV2D
extends V2D {
  constructor(x:number, y:number) {
    super(x, y);
  }
}
