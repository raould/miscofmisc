import { V2D } from './V2D';

export interface IP2Dmk<V extends V2D> {
  zero():IP2D<V>;
}

export interface IP2D<V extends V2D> {
  getPos():V;
  getVel():V;
  getAcc():V;

  putPos(p:V):IP2D<V>;
  putVel(v:V):IP2D<V>;
  putAcc(a:V):IP2D<V>;
}

