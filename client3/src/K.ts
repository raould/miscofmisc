export class K {

  // match: index.html should rougly match this aspect ratio.
  // todo: get SHOW_ALL to work, i want remove this hard-coded scale hack.
  // todo: handle runtime display density.
  // todo: handle display-appropriate letterboxing.
  static readonly scale = 1/3;
  static readonly w2s = (w:number):number => w * K.scale;
  static readonly s2w = (s:number):number => s / K.scale;

  // units: logical 2D world units.
  static readonly w_width = 1024;
  static readonly w_height = 768;
  // units: physical 2D screen units.
  static readonly s_width = K.w2s(K.w_width);
  static readonly s_height = K.w2s(K.w_height);

  static readonly wstep:number = 100; // todo: 
}

