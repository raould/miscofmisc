import { K } from './K';

export interface IV2Dmk<V extends IV2D> {
  zero():V;
}

export interface IV2D {
  x:number;
  y:number;
}
