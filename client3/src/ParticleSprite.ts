import Sprite from 'openfl/display/Sprite';
import Bitmap from 'openfl/display/Bitmap';
import BitmapData from 'openfl/display/BitmapData';
import { K } from './K';
import { V2Dutil, WV2Dmk, WV2D, SV2D } from './V2D';
import { IP2D } from './IP2D';
import { WP2Dmk, WP2D } from './P2D';

export class ParticleSprite
extends Sprite 
implements IP2D<WV2D> {
  
  wp:WP2D;

  constructor () {
    super ();
    // todo: i don't grok Flash Sprite,
    // why is the bitmap data a complete sub child
    // instead of just a setter into this Sprite's bitmap?
    BitmapData
      .loadFromFile("particle.png")
      .onComplete((data) => {
        this.addChild(new Bitmap(data));
      });
    this.wp = new WP2Dmk().zero();
  }

  getPos():WV2D { return this.wp.pos; }
  getVel():WV2D { return this.wp.vel; }
  getAcc():WV2D { return this.wp.acc; }

  putPos(p:WV2D):WP2D {
    this.wp.pos = p;
    this.wp.vel = WV2Dmk.zero();
    this.wp.acc = WV2Dmk.zero();
    const s:SV2D = V2Dutil.w2s(p);
    this.x = s.x;
    this.y = s.y;
    return this.wp;
  }
  putVel(v:WV2D):WP2D { this.wp.vel = v; return this.wp; }
  putAcc(a:WV2D):WP2D { this.wp.acc = a; return this.wp; }
}


