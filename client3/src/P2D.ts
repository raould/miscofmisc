import { K } from './K';
import { IV2Dmk } from './IV2D';
import { V2D, WV2D, SV2D } from './V2D';
import { IP2Dmk, IP2D } from './IP2D';

export class P2D<V extends V2D>
implements IP2D<V> {
  pos:V;
  vel:V;
  acc:V;
  
  constructor(factory:IV2Dmk<V>) {
    this.pos = factory.zero();
    this.vel = factory.zero();
    this.acc = factory.zero();
  }

  getPos():V { return this.pos; }
  getVel():V { return this.vel; }
  getAcc():V { return this.acc; }

  putPos(p:V):P2D<V> { this.pos = p; return this; }
  putVel(v:V):P2D<V> { this.vel = v; return this; }
  putAcc(a:V):P2D<V> { this.acc = a; return this; }
}

export class WP2Dmk
implements IP2Dmk<WV2D> {
  zero():IP2D<WV2D> {
    return new P2D<WV2D>(0, 0);
  }
}

export class SP2Dmk
implements IP2Dmk<SV2D> {
  zero():IP2D<SV2D> {
    return new P2D<SV2D>(0, 0);
  }
}

export class WP2D
extends P2D<WV2D> {
  constructor() {
    super(new WP2Dmk());
  }
}

export class SP2D
extends P2D<SV2D> {
  constructor() {
    super(new SP2Dmk());
  }
}
