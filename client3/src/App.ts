import Sprite from 'openfl/display/Sprite';
import Stage from 'openfl/display/Stage';
import { K } from './K';
import { Field } from './Field';

class App extends Sprite {
  field:Field;
  constructor () {
    super ();
    this.field = new Field();
    this.addChild(this.field);
  }
}

// todo: remove the final App argument, it is just confusing. 
// http://community.openfl.org/t/what-is-the-purpose-of-stages-documentclass/10859/4
var stage = new Stage(K.s_width, K.s_height, 0xFFFFFF, App);
document.body.appendChild(stage.element);
